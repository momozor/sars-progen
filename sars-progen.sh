#!/bin/sh

usage() {
        echo "Usage: $0 NAME [-h, --help]"
}

if [ -z "$1" ]
then
    usage
    exit 1
elif [ ! -z "$1" ]
then
     project_name="$1"
fi

if [ ! -z "$2" ] && [ ! -z "$1" ] && [ $(echo "$2" | grep -E "^-h|--help$") ]
then
    echo "project generation is aborted to show this help screen"
    usage
    exit 1
fi

current_year=$(date +"%Y")
license_notice=$(cat <<-END
--  For license, see COPYING file for further details.
--  Copyright $current_year <NAME> <EMAIL>
END
)

gprbuild_template() {
    (capitalized_pn=$(echo "$project_name" | sed -r 's/\<./\U&/g')
     cat <<EOF > "$project_name/$project_name.gpr"
project $capitalized_pn is
   for Source_Dirs use ("src");
   for Object_Dir use "obj";
   for Exec_Dir use ".";
   for Main use ("main.adb");

   type Build_Type is ("Debug", "Release");
   Build : Build_Type := external ("Build", "Debug");
   Build_Switches := ();
   case Build is
      when "Debug" =>
         Build_Switches := ("-g");
      when "Release" =>
         Build_Switches := ("O3");
   end case;

   package Compiler is 
      for Default_Switches ("ada") use ("-gnat2012", "-gnatwa", "-gnatwe",
                                        "-gnatyg", "-gnatyd", "-gnatata",
                                        "-gnatE") & Build_Switches;
   end Compiler;

   package Builder is
      for Executable ("main.adb") use "$project_name";
   end Builder;
end $capitalized_pn;
EOF
    )
}

main_template() {
    cat <<EOF > "$project_name/src/main.adb"
$license_notice

with Core;

procedure Main is
begin
   Core.Say ("Hello world from $project_name.");
end Main;
EOF
}

gitignore_template() {
    cat <<EOF > "$project_name/.gitignore"
$project_name
obj/
gnatprove/
*.o
*.so
*.a
*.out
gnatinspect.db
EOF
}

makefile_template() {
    (
    makefile_path="$project_name/Makefile"
    gprfile_path="$project_name.gpr"

    printf "GPRBUILD=gprbuild\n" >> "$makefile_path"
    printf "GNATPROVE=gnatprove\n" >> "$makefile_path"
    printf "SOURCE=src\n" >> "$makefile_path"
    printf "OUTPUT=$project_name\n" >> "$makefile_path"
    printf "\n" >> "$makefile_path"
    printf "all: \$(SOURCE)/main.adb $gprfile_path\n" >> "$makefile_path"
    printf "\t\$(GPRBUILD) -p\n"  >> "$makefile_path"
    printf "\n" >> "$makefile_path"
    printf "prove: \$(SOURCE)/main.adb $gprfile_path\n" >> "$makefile_path"
    printf "\t\$(GNATPROVE) -P$gprfile_path\n" >> "$makefile_path"
    printf "\n" >> "$makefile_path"
    printf "clean: \$(OUTPUT)\n" >> "$makefile_path"
    printf "\trm -f \$(OUTPUT)\n" >> "$makefile_path"
    printf "\trm -rf 'obj'\n" >> "$makefile_path"
    printf "\tif [ -d 'gnatprove' ];then rm -rf 'gnatprove'; fi\n" >> "$makefile_path"
    )
}

readme_template() {
    cat <<EOF > "$project_name/README"
$project_name
-------------

License
-------

EOF
}

corefiles_template() {
    cat <<EOF > "$project_name/src/core.ads"
$license_notice

package Core is
   procedure Say (Text : String);
end Core;
EOF

    cat <<EOF > "$project_name/src/core.adb"
$license_notice

with Ada.Text_IO;

package body Core is
   package IO renames Ada.Text_IO;
   procedure Say (Text : String) is
   begin
      IO.Put_Line (Text);
   end Say;
end Core;
EOF
}

if [ $(echo "$project_name" | grep -o '^[[:alpha:]]') ]
then
    mkdir -p "$project_name/src"
    gprbuild_template
    main_template
    gitignore_template
    makefile_template
    readme_template
    corefiles_template
else
    echo "INCORRECT"
fi

